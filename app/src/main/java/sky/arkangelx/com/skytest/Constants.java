package sky.arkangelx.com.skytest;

/**
 * Created by arkangel on 23/06/2017.
 */

public class Constants {
    public static final String FIREBASE_BASE_URL="https://skytest-98258.firebaseio.com/";
    public static final String FIREBASE_SKY_REWARDS = "rewards";
    public static final String FIREBASE_SKYCUSTOMERS_ROOT = "sky_customers";
    public static final String FIREBASE_SKYCUSTOMERS_ROOT_UPDATE = "skycustomers";
    public static final String FIREBASE_SKYCUSTOMERS="customers";
    public static final String FIREBASE_SKY_REWARDS_OPTIONS="rewardoptions";
    public static final String FILE_NAME_GENERATED_JSON="generated.json";
    public static final String FILE_NAME_REWARDS_JSON="rewards_data.json";
    public static final class Injection {
        private Injection() {
        }

        public static final class Named {
            private Named() {
            }

            public static final String KEY_SKY_OKHTTP = "SkyOkhttp";
            public static final String KEY_BASE_URL = "SkyBaseUrl";
            public static final String SERVICE_BASE_URL = "https://private-0bf45c-sky14.apiary-mock.com";
            public static final String KEY_FIREBASE_BASE_URL = "FirebaseEndPoint";
            public static final String KEY_SKY_RETROFIT="SkyRetrofit";

        }

    }
}
