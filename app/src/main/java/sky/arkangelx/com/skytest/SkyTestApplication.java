package sky.arkangelx.com.skytest;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.frogermcs.androiddevmetrics.AndroidDevMetrics;
import com.squareup.leakcanary.LeakCanary;

import sky.arkangelx.com.skytest.datasource.modules.ApplicationComponent;
import sky.arkangelx.com.skytest.datasource.modules.ApplicationModule;
import sky.arkangelx.com.skytest.datasource.modules.DaggerApplicationComponent;
import sky.arkangelx.com.skytest.datasource.modules.DataModule;
import timber.log.Timber;


public class SkyTestApplication extends Application {

    static Context context;
    private static ApplicationComponent component;


    public static SkyTestApplication getInstance() {
        return instance;
    }

    private static SkyTestApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        this.setAppContext(getApplicationContext());
        //dependency injection
        instance = this;
        Timber.plant(new Timber.DebugTree());
        if (BuildConfig.DEBUG) {
            AndroidDevMetrics.initWith(this);
            if (LeakCanary.isInAnalyzerProcess(this)) {
                // This process is dedicated to LeakCanary for heap analysis.
                // You should not init your app in this process.
                return;
            }
            LeakCanary.install(this);
        }
        component = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(getInstance())).dataModule(new DataModule(getInstance()))
                .build();


    }


    public void setAppContext(Context mAppContext) {
        SkyTestApplication.context = mAppContext;
    }


    public static ApplicationComponent getAppComponent() {
        return component;
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }
}
