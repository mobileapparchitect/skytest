package sky.arkangelx.com.skytest.datasource.endpoints;

import retrofit2.http.GET;
import rx.Observable;
import sky.arkangelx.com.skytest.datasource.model.Reward;
import sky.arkangelx.com.skytest.datasource.model.RewardOptionsContainer;
import sky.arkangelx.com.skytest.datasource.model.Rewardoptions;
import sky.arkangelx.com.skytest.datasource.model.SkyCustomerContainer;
import sky.arkangelx.com.skytest.datasource.model.SkyCustomers;

public interface CustomersApiService {

    // https://private-0bf45c-sky14.apiary-mock.com/customers
    @GET("/customers")
    Observable<SkyCustomerContainer> getCustomers();


    //https://private-0bf45c-sky14.apiary-mock.com/entitlements
    @GET("/entitlements")
    Observable<Rewardoptions> getRewards();


}

