
package sky.arkangelx.com.skytest.datasource.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Customer {

    @JsonProperty("address")
    @SerializedName("address")
    @Expose
    private String address;
    @JsonProperty("createddate")
    @SerializedName("createddate")
    @Expose
    private String createddate;
    @SerializedName("email")
    @JsonProperty("email")
    @Expose
    private String email;
    @SerializedName("company")
    @JsonProperty("company")
    @Expose
    private String company;
    @JsonProperty("eligibilityid")
    @SerializedName("eligibilityid")
    @Expose
    private Integer eligibilityid;
    @SerializedName("phone")
    @JsonProperty("phone")
    @Expose
    private String phone;
    @SerializedName("accountnumber")
    @JsonProperty("accountnumber")
    @Expose
    private Integer accountnumber;
    @SerializedName("name")
    @JsonProperty("name")
    @Expose
    private Name name;

    public String getRewardErrorMesage() {
        return rewardErrorMesage;
    }

    public void setRewardErrorMesage(String rewardErrorMesage) {
        this.rewardErrorMesage = rewardErrorMesage;
    }

    private String rewardErrorMesage;
    public Reward getRewardsDatum() {
        return rewardsDatum;
    }

    public void setRewardsDatum(Reward rewardsDatum) {
        this.rewardsDatum = rewardsDatum;
    }

    private Reward rewardsDatum;

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCreateddate() {
        return createddate;
    }

    public void setCreateddate(String createddate) {
        this.createddate = createddate;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCompany() {
        return company;
    }

    public void setCompany(String company) {
        this.company = company;
    }

    public Integer getEligibilityid() {
        return eligibilityid;
    }

    public void setEligibilityid(Integer eligibilityid) {
        this.eligibilityid = eligibilityid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public Integer getAccountnumber() {
        return accountnumber;
    }

    public void setAccountnumber(Integer accountnumber) {
        this.accountnumber = accountnumber;
    }

    public Name getName() {
        return name;
    }

    public void setName(Name name) {
        this.name = name;
    }

}
