
package sky.arkangelx.com.skytest.datasource.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Name {

    @SerializedName("last")
    @JsonProperty("last")
    @Expose
    private String last;
    @JsonProperty("first")
    @SerializedName("first")
    @Expose
    private String first;

    public String getLast() {
        return last;
    }

    public void setLast(String last) {
        this.last = last;
    }

    public String getFirst() {
        return first;
    }

    public void setFirst(String first) {
        this.first = first;
    }

}
