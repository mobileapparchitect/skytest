
package sky.arkangelx.com.skytest.datasource.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Reward {

    @JsonProperty("channel")
    @SerializedName("channel")
    @Expose
    private String channel;
    @JsonProperty("reward")
    @SerializedName("reward")
    @Expose
    private String reward;
    @JsonProperty("rewardId")
    @SerializedName("rewardId")
    @Expose
    private String rewardId;

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getReward() {
        return reward;
    }

    public void setReward(String reward) {
        this.reward = reward;
    }

    public String getRewardId() {
        return rewardId;
    }

    public void setRewardId(String rewardId) {
        this.rewardId = rewardId;
    }

}
