
package sky.arkangelx.com.skytest.datasource.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RewardOptionsContainer {

    @SerializedName("rewardoptions")
    @Expose
    @JsonProperty("rewardoptions")
    private Rewardoptions rewardoptions;

    public Rewardoptions getRewardoptions() {
        return rewardoptions;
    }

    public void setRewardoptions(Rewardoptions rewardoptions) {
        this.rewardoptions = rewardoptions;
    }

}
