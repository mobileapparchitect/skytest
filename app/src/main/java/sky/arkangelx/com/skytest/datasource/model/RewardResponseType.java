package sky.arkangelx.com.skytest.datasource.model;

public enum RewardResponseType {
    CUSTOMER_ELIGIBLE,
    CUSTOMER_INELIGIBLE,
    TECHNICAL_FAILURE, INVALID_ACCOUNT_NUMBER
}