
package sky.arkangelx.com.skytest.datasource.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Rewardoptions {

    @SerializedName("rewards")
    @JsonProperty("rewards")
    @Expose
    private List<Reward> rewards = null;

    public List<Reward> getRewards() {
        return rewards;
    }

    public void setRewards(List<Reward> rewards) {
        this.rewards = rewards;
    }

}
