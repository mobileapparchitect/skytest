
package sky.arkangelx.com.skytest.datasource.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SkyCustomerContainer {
    @JsonProperty("skycustomers")
    @SerializedName("skycustomers")
    @Expose
    private SkyCustomers skyCustomers;

    public SkyCustomers getSkyCustomers() {
        return skyCustomers;
    }

    public void setSkyCustomers(SkyCustomers skyCustomers) {
        this.skyCustomers = skyCustomers;
    }

}
