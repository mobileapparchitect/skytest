package sky.arkangelx.com.skytest.datasource.modules;


import dagger.Component;
import sky.arkangelx.com.skytest.presentation.activities.SplashActivity;


@Component(modules = {ApplicationModule.class, DataModule.class})
public interface ApplicationComponent {
    void inject(SplashActivity target);

}