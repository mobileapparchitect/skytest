package sky.arkangelx.com.skytest.datasource.modules;

import android.content.Context;

import com.firebase.client.Firebase;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import javax.inject.Named;

import dagger.Module;
import dagger.Provides;
import okhttp3.CacheControl;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import sky.arkangelx.com.skytest.BuildConfig;
import sky.arkangelx.com.skytest.Constants;
import sky.arkangelx.com.skytest.datasource.endpoints.CustomersApiService;
import sky.arkangelx.com.skytest.datasource.repository.DataRepositoryFactory;
import sky.arkangelx.com.skytest.datasource.repository.FirebaseDataSourceRestClientUseCase;
import sky.arkangelx.com.skytest.datasource.repository.LocalDataSourceClientUseCase;
import sky.arkangelx.com.skytest.datasource.repository.SkyDataSourceRestClientUseCase;

@Module
public class DataModule {

    Context mContext;

    public DataModule(Context context) {
        mContext = context;
    }

    @Provides
    Gson provideGson() {
        return new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
    }

    @Provides
    @Named(Constants.Injection.Named.KEY_SKY_OKHTTP)
    public OkHttpClient provideLoggingCapableHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(BuildConfig.DEBUG ? HttpLoggingInterceptor.Level.BODY : HttpLoggingInterceptor.Level.NONE);
        return new OkHttpClient.Builder().addInterceptor(logging).addInterceptor(new Interceptor() {
            @Override
            public Response intercept(Chain chain) throws IOException {
                Request request = chain.request().newBuilder().addHeader("Content-Type", "application/json")
                        .cacheControl(new CacheControl.Builder().maxStale(365, TimeUnit.DAYS).build()).build();
                return chain.proceed(request);
            }
        })
                .addInterceptor(logging)
                .build();
    }

    @Provides
    @Named(Constants.Injection.Named.KEY_BASE_URL)
    public String provideBaseUrl() {
        return Constants.Injection.Named.SERVICE_BASE_URL;
    }


    @Provides
    @Named(Constants.Injection.Named.KEY_FIREBASE_BASE_URL)
    public String provideFirebaseBaseUrl() {
        return Constants.FIREBASE_BASE_URL;
    }


    @Provides
    @Named(Constants.Injection.Named.KEY_SKY_RETROFIT)
    public Retrofit provideYoutbuteRetrofit(@Named(Constants.Injection.Named.KEY_SKY_OKHTTP) OkHttpClient okHttpClient, @Named(Constants.Injection.Named.KEY_BASE_URL) String baseUrl, Gson gson) {
        return new Retrofit.Builder()
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .baseUrl(baseUrl)
                .client(okHttpClient)
                .build();
    }

    @Provides
    public CustomersApiService providesCustomersApiService( @Named(Constants.Injection.Named.KEY_SKY_RETROFIT) Retrofit retrofit) {
        return retrofit.create(CustomersApiService.class);
    }



    @Provides
    public Firebase providesFirebase(@Named(Constants.Injection.Named.KEY_FIREBASE_BASE_URL) String baseUrl) {
        if (!Firebase.getDefaultConfig().isPersistenceEnabled())
            Firebase.getDefaultConfig().setPersistenceEnabled(true);
        Firebase.setAndroidContext(mContext);
        return new Firebase(baseUrl);
    }


    @Provides
    public SkyDataSourceRestClientUseCase providesSkyDataSourceRestClient(CustomersApiService customersApiService) {
        return new SkyDataSourceRestClientUseCase(customersApiService);

    }

    @Provides
    public LocalDataSourceClientUseCase providesLocalDataSourceClientUseCase(Context context,Gson gson) {
        return new LocalDataSourceClientUseCase(context,gson);

    }

    @Provides
    public FirebaseDataSourceRestClientUseCase providesFirebaseDataSourceRestClientUseCase(Firebase firebase) {
        return new FirebaseDataSourceRestClientUseCase(firebase);

    }

    @Provides
    public DataRepositoryFactory providesDataRepositoryFactory(FirebaseDataSourceRestClientUseCase firebaseDataSourceRestClientUseCase, LocalDataSourceClientUseCase localDataSourceClientUseCase, SkyDataSourceRestClientUseCase skyDataSourceRestClientUseCase) {
        return new DataRepositoryFactory(localDataSourceClientUseCase, skyDataSourceRestClientUseCase, firebaseDataSourceRestClientUseCase);

    }


}