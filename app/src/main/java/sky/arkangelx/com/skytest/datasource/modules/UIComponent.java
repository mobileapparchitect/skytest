package sky.arkangelx.com.skytest.datasource.modules;


import dagger.Component;
import sky.arkangelx.com.skytest.presentation.activities.BaseActivity;
import sky.arkangelx.com.skytest.presentation.fragments.FirebaseFragment;
import sky.arkangelx.com.skytest.presentation.fragments.LocalDataSourceFragment;
import sky.arkangelx.com.skytest.presentation.fragments.SkyCustomerEntitlementFragment;
import sky.arkangelx.com.skytest.presentation.fragments.SkyCustomerEntitlementTabbedFragment;

/**
 * Created by arkangel on 27/04/2017.
 */


@Component(modules = {ApplicationModule.class, DataModule.class, UIModule.class})

//@UIScope
public interface UIComponent {

    void inject(BaseActivity baseActivity);

    void inject(SkyCustomerEntitlementFragment skyCustomerEntitlementFragment);

    void inject(SkyCustomerEntitlementTabbedFragment groceryStoreFragment);

    void inject(LocalDataSourceFragment localDataSourceFragment);

    void inject(FirebaseFragment groceryStoreFragment);

}
