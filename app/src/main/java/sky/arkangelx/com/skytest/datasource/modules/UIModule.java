package sky.arkangelx.com.skytest.datasource.modules;

import android.view.View;

import dagger.Module;
import dagger.Provides;
import sky.arkangelx.com.skytest.datasource.repository.DataRepositoryFactory;
import sky.arkangelx.com.skytest.presentation.presenters.FirebaseDataSourceEntitlementPresenter;
import sky.arkangelx.com.skytest.presentation.presenters.IView;
import sky.arkangelx.com.skytest.presentation.presenters.LocalDataSourceEntitlementPresenter;
import sky.arkangelx.com.skytest.presentation.presenters.SkyCustomerEntitlementView;
import sky.arkangelx.com.skytest.presentation.presenters.SkyEntitlementPresenter;

/**
 * Created by arkangel on 27/04/2017.
 */

@Module
public class UIModule {

    private View mView;
    private IView mViewInterface;

    public UIModule(View view, IView viewInterface) {
        mView = view;
        mViewInterface = viewInterface;
    }


    @Provides
    SkyEntitlementPresenter providesHighlightsPresenter(DataRepositoryFactory dataRepositoryFactory) {
        return new SkyEntitlementPresenter((SkyCustomerEntitlementView) mViewInterface, dataRepositoryFactory);
    }

    @Provides
    FirebaseDataSourceEntitlementPresenter providesFirebaseDataSourceEntitlementPresenter(DataRepositoryFactory dataRepositoryFactory) {
        return new FirebaseDataSourceEntitlementPresenter((SkyCustomerEntitlementView) mViewInterface, dataRepositoryFactory);
    }

    @Provides
    LocalDataSourceEntitlementPresenter providesLocalDataSourceEntitlementPresenter(DataRepositoryFactory dataRepositoryFactory) {
        return new LocalDataSourceEntitlementPresenter((SkyCustomerEntitlementView) mViewInterface, dataRepositoryFactory);
    }


}