package sky.arkangelx.com.skytest.datasource.repository;

public class DataRepositoryFactory {
    private LocalDataSourceClientUseCase mLocalDataSourceClientUseCase;
    private SkyDataSourceRestClientUseCase mSkyDataSourceRestClientUseCase;
    private FirebaseDataSourceRestClientUseCase mFirebaseDataSourceRestClientUseCase;

    public DataRepositoryFactory(LocalDataSourceClientUseCase localDataSourceClientUseCase, SkyDataSourceRestClientUseCase skyDataSourceRestClientUseCase, FirebaseDataSourceRestClientUseCase firebaseDataSourceRestClientUseCase) {
        this.mFirebaseDataSourceRestClientUseCase = firebaseDataSourceRestClientUseCase;
        this.mLocalDataSourceClientUseCase = localDataSourceClientUseCase;
        this.mSkyDataSourceRestClientUseCase = skyDataSourceRestClientUseCase;
    }

    public SkyDataSourceRestClient getRepoType(RepoType restClientType) {

        switch (restClientType) {
            case LOCAL_SOURCE:
                return mLocalDataSourceClientUseCase;
            case HTPPS_SOURCE:
                return mSkyDataSourceRestClientUseCase;
            case FIREBASE_SOURCE:
                return mFirebaseDataSourceRestClientUseCase;
        }

        return null;
    }
}