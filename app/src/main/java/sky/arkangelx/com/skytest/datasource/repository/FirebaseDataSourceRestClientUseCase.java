package sky.arkangelx.com.skytest.datasource.repository;

import android.util.Log;

import com.firebase.client.Firebase;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.Query;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import sky.arkangelx.com.skytest.Constants;
import sky.arkangelx.com.skytest.datasource.model.Customer;
import sky.arkangelx.com.skytest.datasource.model.Reward;
import sky.arkangelx.com.skytest.datasource.model.RewardResponseType;

import static sky.arkangelx.com.skytest.Constants.FIREBASE_SKYCUSTOMERS;
import static sky.arkangelx.com.skytest.Constants.FIREBASE_SKY_REWARDS_OPTIONS;

/**
 * Created by arkangel on 23/06/2017.
 */

public class FirebaseDataSourceRestClientUseCase implements SkyDataSourceRestClient {
    private Firebase mFirebase;
    private List<Customer> tempList;
    private RewardResponseType rewardResponseType;

    public FirebaseDataSourceRestClientUseCase(Firebase firebase) {
        mFirebase = firebase;
    }

    @Override
    public Observable<List<Customer>> getCustomers(int startIndex, int endIndex) {
        return Observable.zip(retrieveCustomersFromFirebase(), retrieveRewardsFromFireBase(), (customerList, rewardsData) -> {
            tempList = new ArrayList<>();
            Observable.from(customerList.subList(startIndex, endIndex)).observeOn(AndroidSchedulers.mainThread()).forEach(customer -> {
                String[] dateParts = customer.getCreateddate().split(",");
                String monthPart = dateParts[1].replaceAll("\\d", "").trim();
                if (monthPart.equals("January") || monthPart.equals("February") || monthPart.equals("March")) {
                    rewardResponseType = RewardResponseType.CUSTOMER_ELIGIBLE;
                } else if (monthPart.equals("April") || monthPart.equals("May") || monthPart.equals("June")) {
                    rewardResponseType = RewardResponseType.CUSTOMER_INELIGIBLE;
                } else if (monthPart.equals("July") || monthPart.equals("August") || monthPart.equals("September")) {
                    rewardResponseType = RewardResponseType.TECHNICAL_FAILURE;
                } else if (monthPart.equals("October") || monthPart.equals("November") || monthPart.equals("December")) {
                    rewardResponseType = RewardResponseType.INVALID_ACCOUNT_NUMBER;
                }

                switch (rewardResponseType) {
                    case INVALID_ACCOUNT_NUMBER:
                        customer.setRewardErrorMesage("The Supplied account number is invalid");
                        tempList.add(customer);
                        break;
                    case TECHNICAL_FAILURE:
                        customer.setRewardErrorMesage("Service technical failure");
                        tempList.add(customer);
                        break;
                    case CUSTOMER_ELIGIBLE:
                        for (Reward reward : rewardsData) {
                            if (reward.getRewardId().equals(String.valueOf(customer.getEligibilityid()))) {
                                customer.setRewardsDatum(reward);
                                tempList.add(customer);
                            }
                        }
                        break;
                    case CUSTOMER_INELIGIBLE:
                        customer.setRewardErrorMesage("Customer is not eligible");
                        tempList.add(customer);
                        break;
                }

            });
            return tempList;
        }).switchMap(customers -> Observable.just(customers));


    }


    public Observable<List<Customer>> retrieveCustomersFromFirebase() {
        return Observable.create(subscriber -> {
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
            Query query = reference.child(Constants.FIREBASE_SKYCUSTOMERS_ROOT_UPDATE).child(FIREBASE_SKYCUSTOMERS);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                    GenericTypeIndicator<List<Customer>> genericTypeIndicator = new GenericTypeIndicator<List<Customer>>() {
                    };
                    List<Customer> customers = dataSnapshot.getValue(genericTypeIndicator);
                    subscriber.onNext(customers);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(FirebaseDataSourceRestClientUseCase.class.getSimpleName(), databaseError.getMessage().toString());
                    subscriber.onError(new Throwable(databaseError.getMessage()));
                }
            });


        });
    }

    public Observable<List<Reward>> retrieveRewardsFromFireBase() {
        return Observable.create(subscriber -> {
            DatabaseReference reference = FirebaseDatabase.getInstance().getReference();
            Query query = reference.child(Constants.FIREBASE_SKYCUSTOMERS_ROOT_UPDATE).child(FIREBASE_SKY_REWARDS_OPTIONS);
            query.addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(com.google.firebase.database.DataSnapshot dataSnapshot) {
                    DataSnapshot items = dataSnapshot.child(Constants.FIREBASE_SKY_REWARDS);
                    GenericTypeIndicator<List<Reward>> t = new GenericTypeIndicator<List<Reward>>() {
                    };
                    List<Reward> rewardsData = items.getValue(t);
                    subscriber.onNext(rewardsData);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    Log.e(FirebaseDataSourceRestClientUseCase.class.getSimpleName(), databaseError.getMessage().toString());
                    subscriber.onError(new Throwable(databaseError.getMessage()));
                }
            });


        });


    }

}