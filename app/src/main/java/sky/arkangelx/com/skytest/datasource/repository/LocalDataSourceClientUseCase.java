package sky.arkangelx.com.skytest.datasource.repository;

import android.content.Context;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action1;
import rx.functions.Func1;
import rx.functions.Func2;
import sky.arkangelx.com.skytest.Constants;
import sky.arkangelx.com.skytest.datasource.model.Customer;
import sky.arkangelx.com.skytest.datasource.model.Reward;
import sky.arkangelx.com.skytest.datasource.model.RewardResponseType;
import sky.arkangelx.com.skytest.datasource.model.Rewardoptions;
import sky.arkangelx.com.skytest.datasource.model.SkyCustomerContainer;
import sky.arkangelx.com.skytest.utils.FileUtils;

public class LocalDataSourceClientUseCase implements SkyDataSourceRestClient {
    private Context mContext;
    private ArrayList<Customer> arrayListOfCustomers;
    private ArrayList<Reward> rewardArrayList;
    private Gson mGson;
    private List<Customer> tempList;
    private RewardResponseType rewardResponseType;

    public LocalDataSourceClientUseCase(Context context, Gson gson) {
        mContext = context;
        arrayListOfCustomers = new ArrayList<>();
        rewardArrayList = new ArrayList<>();
        tempList = new ArrayList<>();
        mGson = gson;
        init();
    }


    @Override
    public Observable<List<Customer>> getCustomers(int startIndex, int endIndex) {

        Observable.from(arrayListOfCustomers.subList(startIndex, endIndex)).observeOn(AndroidSchedulers.mainThread()).forEach(customer -> {
            String[] dateParts = customer.getCreateddate().split(",");
            String monthPart = dateParts[1].replaceAll("\\d", "").trim();
            if (monthPart.equals("January") || monthPart.equals("February") || monthPart.equals("March")) {
                rewardResponseType = RewardResponseType.CUSTOMER_ELIGIBLE;
            } else if (monthPart.equals("April") || monthPart.equals("May") || monthPart.equals("June")) {
                rewardResponseType = RewardResponseType.CUSTOMER_INELIGIBLE;
            } else if (monthPart.equals("July") || monthPart.equals("August") || monthPart.equals("September")) {
                rewardResponseType = RewardResponseType.TECHNICAL_FAILURE;
            } else if (monthPart.equals("October") || monthPart.equals("November") || monthPart.equals("December")) {
                rewardResponseType = RewardResponseType.INVALID_ACCOUNT_NUMBER;
            }

            switch (rewardResponseType) {
                case INVALID_ACCOUNT_NUMBER:
                    customer.setRewardErrorMesage("The Supplied account number is invalid");
                    tempList.add(customer);
                    break;
                case TECHNICAL_FAILURE:
                    customer.setRewardErrorMesage("Service technical failure");
                    tempList.add(customer);
                    break;
                case CUSTOMER_ELIGIBLE:
                    for (Reward reward : rewardArrayList) {
                        if (reward.getRewardId().equals(String.valueOf(customer.getEligibilityid()))) {
                            customer.setRewardsDatum(reward);
                            tempList.add(customer);
                        }
                    }
                    break;
                case CUSTOMER_INELIGIBLE:
                    customer.setRewardErrorMesage("Customer is not eligible");
                    tempList.add(customer);
                    break;
            }
        });
        return Observable.just(tempList);
    }


    private void init() {
        Observable.zip(loadCustomerData(), loadRewardData(), new Func2<List<Customer>, List<Reward>, Void>() {
            @Override
            public Void call(List<Customer> customers, List<Reward> rewards) {
                rewardArrayList.addAll(rewards);
                arrayListOfCustomers.addAll(customers);
                return null;
            }
        }).subscribe(aVoid -> {

        });

    }

    private Observable<List<Customer>> loadCustomerData() {
        return FileUtils.readFileAsStringObservable(mContext, Constants.FILE_NAME_GENERATED_JSON).switchMap(new Func1<String, Observable<List<Customer>>>() {
            @Override
            public Observable<List<Customer>> call(String s) {
                List<Customer> items = mGson.fromJson(s, SkyCustomerContainer.class).getSkyCustomers().getCustomers();
                return Observable.just(items);
            }
        });
    }

    private Observable<List<Reward>> loadRewardData() {
        return FileUtils.readFileAsStringObservable(mContext, Constants.FILE_NAME_REWARDS_JSON).switchMap(new Func1<String, Observable<List<Reward>>>() {
            @Override
            public Observable<List<Reward>> call(String s) {
                List<Reward> rewardsList = mGson.fromJson(s, Rewardoptions.class).getRewards();
                return Observable.just(rewardsList);
            }
        });


    }
}
