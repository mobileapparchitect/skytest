package sky.arkangelx.com.skytest.datasource.repository;

public enum RepoType {
    LOCAL_SOURCE,
    FIREBASE_SOURCE,
    HTPPS_SOURCE
}