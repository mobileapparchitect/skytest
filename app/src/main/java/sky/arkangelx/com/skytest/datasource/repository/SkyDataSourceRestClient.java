package sky.arkangelx.com.skytest.datasource.repository;

import java.util.List;

import rx.Observable;
import sky.arkangelx.com.skytest.datasource.model.Customer;


public interface SkyDataSourceRestClient {


    Observable<List<Customer>> getCustomers(int startIndex, int endIndex);


}
