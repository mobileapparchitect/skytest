package sky.arkangelx.com.skytest.datasource.repository;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import sky.arkangelx.com.skytest.datasource.endpoints.CustomersApiService;
import sky.arkangelx.com.skytest.datasource.model.Customer;
import sky.arkangelx.com.skytest.datasource.model.Reward;
import sky.arkangelx.com.skytest.datasource.model.RewardResponseType;
import sky.arkangelx.com.skytest.datasource.model.SkyCustomerContainer;

/**
 * Created by arkangel on 23/06/2017.
 */

public class SkyDataSourceRestClientUseCase implements SkyDataSourceRestClient {

    private CustomersApiService mCustomersApiService;
    private ArrayList tempList;
    private RewardResponseType rewardResponseType;

    public SkyDataSourceRestClientUseCase(CustomersApiService customersApiService) {
        this.mCustomersApiService = customersApiService;
        tempList = new ArrayList();
    }


    //this is a bit stupid repeating code in the use cases

    public Observable<List<Customer>> getCustomers(int startIndex, int endIndex) {
        return Observable.zip(retriveCustomersFromHttpSource(), retrieveRewardsFromHttpSource(), (skyCustomerContainer, rewardsData) -> {
            tempList = new ArrayList<>();
            Observable.from(skyCustomerContainer.getSkyCustomers().getCustomers().subList(startIndex, endIndex)).forEach(customer -> {
                String[] dateParts = customer.getCreateddate().split(",");
                String monthPart = dateParts[1].replaceAll("\\d", "").trim();

                if (monthPart.equals("January") || monthPart.equals("February") || monthPart.equals("March")) {
                    rewardResponseType = RewardResponseType.CUSTOMER_ELIGIBLE;
                } else if (monthPart.equals("April") || monthPart.equals("May") || monthPart.equals("June")) {
                    rewardResponseType = RewardResponseType.CUSTOMER_INELIGIBLE;
                } else if (monthPart.equals("July") || monthPart.equals("August") || monthPart.equals("September")) {
                    rewardResponseType = RewardResponseType.TECHNICAL_FAILURE;
                } else if (monthPart.equals("October") || monthPart.equals("November") || monthPart.equals("December")) {
                    rewardResponseType = RewardResponseType.INVALID_ACCOUNT_NUMBER;
                }

                switch (rewardResponseType) {
                    case INVALID_ACCOUNT_NUMBER:
                        customer.setRewardErrorMesage("The Supplied account number is invalid");
                        tempList.add(customer);
                        break;
                    case TECHNICAL_FAILURE:
                        customer.setRewardErrorMesage("Service technical failure");
                        tempList.add(customer);
                        break;
                    case CUSTOMER_ELIGIBLE:
                        for (Reward reward : rewardsData) {
                            if (reward.getRewardId().equals(String.valueOf(customer.getEligibilityid()))) {
                                customer.setRewardsDatum(reward);
                                tempList.add(customer);
                            }
                        }
                        break;
                    case CUSTOMER_INELIGIBLE:
                        customer.setRewardErrorMesage("Customer is not eligible");
                        tempList.add(customer);
                        break;
                }
            });
            return tempList;
        });


    }

    private Observable<SkyCustomerContainer> retriveCustomersFromHttpSource() {
        return this.mCustomersApiService.getCustomers();
    }

    private Observable<List<Reward>> retrieveRewardsFromHttpSource() {
        return this.mCustomersApiService.getRewards().switchMap(rewardoptions -> Observable.just(rewardoptions.getRewards()));
    }

}
