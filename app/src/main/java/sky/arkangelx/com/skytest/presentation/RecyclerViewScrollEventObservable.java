package sky.arkangelx.com.skytest.presentation;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.jakewharton.rxbinding.support.v7.widget.RecyclerViewScrollEvent;
import com.jakewharton.rxbinding.support.v7.widget.RxRecyclerView;

import rx.Observable;
import rx.Subscriber;
import rx.Subscription;

public class RecyclerViewScrollEventObservable implements Observable.OnSubscribe<RecyclerViewScrollEvent> {

    private RecyclerView mRecycler;
    private LinearLayoutManager mLinearLayoutManger;
    private Subscription subscription;

    public static Observable<RecyclerViewScrollEvent> createObservable(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager) {
        return Observable.create(new RecyclerViewScrollEventObservable(recyclerView, linearLayoutManager));
    }

    public RecyclerViewScrollEventObservable(RecyclerView recyclerView, LinearLayoutManager linearLayoutManager) {
        mRecycler = recyclerView;
        mLinearLayoutManger = linearLayoutManager;
    }


    @Override
    public void call(Subscriber<? super RecyclerViewScrollEvent> subscriber) {
      if(subscription!=null && !subscription.isUnsubscribed()){
          subscription.unsubscribe();
      }
        subscription = RxRecyclerView.scrollEvents(mRecycler).subscribe(event -> {
            int totalItemCount = mLinearLayoutManger.getItemCount();
            int lastVisibleItemPosition = mLinearLayoutManger.findLastVisibleItemPosition();
            if (totalItemCount - 1 <= lastVisibleItemPosition) {
                subscriber.onNext(event);
            }
        });
    }
}