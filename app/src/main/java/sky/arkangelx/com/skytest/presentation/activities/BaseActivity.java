package sky.arkangelx.com.skytest.presentation.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import sky.arkangelx.com.skytest.R;
import sky.arkangelx.com.skytest.SkyTestApplication;
import sky.arkangelx.com.skytest.datasource.modules.ApplicationModule;

import sky.arkangelx.com.skytest.datasource.modules.DaggerUIComponent;
import sky.arkangelx.com.skytest.datasource.modules.DataModule;
import sky.arkangelx.com.skytest.datasource.modules.UIComponent;
import sky.arkangelx.com.skytest.datasource.modules.UIModule;
import sky.arkangelx.com.skytest.presentation.presenters.BasePresenter;
import sky.arkangelx.com.skytest.presentation.presenters.IView;

public abstract class BaseActivity extends AppCompatActivity implements IView {
    private Unbinder unbinder;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Nullable
    @BindView(R.id.ivLogo)
    ImageView ivLogo;


    private final CompositeDisposable disposables = new CompositeDisposable();

    @Override
    public void setContentView(int layoutResID) {
        super.setContentView(layoutResID);
        injectViews();
        inject(getUIComponent(findViewById(android.R.id.content).getRootView()));
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }


    protected void injectViews() {
        unbinder = ButterKnife.bind(this);
        setupToolbar();
    }

    public void setContentViewWithoutInject(int layoutResId) {
        super.setContentView(layoutResId);
    }


    protected void setupToolbar() {
        if (toolbar != null) {
           // setSupportActionBar(toolbar);

        }
    }


    public Toolbar getToolbar() {
        return toolbar;
    }


    public ImageView getIvLogo() {
        return ivLogo;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (unbinder != null) {
            unbinder.unbind();
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
    }


    protected UIComponent getUIComponent(View rootView) {
        final UIModule uIModule = new UIModule(rootView, this);
        return DaggerUIComponent.builder().dataModule(new DataModule(SkyTestApplication.getInstance())).applicationModule(new ApplicationModule(SkyTestApplication.getInstance())).uIModule(uIModule).build();

    }


    protected abstract void inject(UIComponent uiComponent);

    protected abstract int getLayoutResId();

    protected abstract BasePresenter getPresenter();


    protected void showErrorMessage(int stringResource) {
        ViewGroup viewGroup = (ViewGroup) findViewById(android.R.id.content).getRootView();
        final Snackbar snackbar = Snackbar.make(viewGroup.getChildAt(0), getResources().getString(stringResource), Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(android.R.string.ok, v -> snackbar.dismiss());
        snackbar.show();
    }


    protected final void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }
}
