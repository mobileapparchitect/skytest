package sky.arkangelx.com.skytest.presentation.activities;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.ColorInt;
import android.support.annotation.ColorRes;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;

import butterknife.BindView;
import sky.arkangelx.com.skytest.R;
import sky.arkangelx.com.skytest.datasource.modules.UIComponent;
import sky.arkangelx.com.skytest.presentation.fragments.SkyCustomerEntitlementTabbedFragment;
import sky.arkangelx.com.skytest.presentation.presenters.BasePresenter;

/**
 * Created by arkangel on 01/05/2017.
 */

public class MainActivity extends BaseActivity {

    @BindView(R.id.appbar)
    AppBarLayout appBarLayout;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(getLayoutResId());
        AppBarLayout.LayoutParams params = (AppBarLayout.LayoutParams) toolbar.getLayoutParams();
        params.setScrollFlags(0);
        getSupportFragmentManager().beginTransaction().replace(R.id.fragment_container, SkyCustomerEntitlementTabbedFragment.getInstance()).commit();


    }

    @Override
    protected void inject(UIComponent uiComponent) {

    }

    @Override
    protected int getLayoutResId() {
        return R.layout.main_activity;
    }

    @Override
    protected BasePresenter getPresenter() {
        return null;
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    public static Intent intentOf(Context context) {
        return new Intent(context, MainActivity.class);
    }


    @ColorInt
    private int color(@ColorRes int res) {
        return ContextCompat.getColor(this, res);
    }
}
