package sky.arkangelx.com.skytest.presentation.activities;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by arkangel on 22/06/2017.
 */

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        SplashActivity.this.startActivity(MainActivity.intentOf(SplashActivity.this));
        SplashActivity.this.finish();
    }
}
