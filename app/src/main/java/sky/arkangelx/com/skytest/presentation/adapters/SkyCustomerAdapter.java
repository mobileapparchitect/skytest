package sky.arkangelx.com.skytest.presentation.adapters;

import android.content.Context;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.jakewharton.rxbinding.view.RxView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import sky.arkangelx.com.skytest.R;
import sky.arkangelx.com.skytest.datasource.model.Customer;
import sky.arkangelx.com.skytest.presentation.presenters.BasePresenter;
import sky.arkangelx.com.skytest.presentation.widgets.SeparatedTextWidget;


public class SkyCustomerAdapter extends RecyclerView.Adapter<SkyCustomerAdapter.ViewHolder> {


    public static Context mContext;
    private List<Customer> customerList;
    private BasePresenter mPresenter;

    public SkyCustomerAdapter(Context context, BasePresenter presenter) {
        mContext = context;
        customerList = new ArrayList<>();
        mPresenter = presenter;
    }

    public void addItems(List<Customer> items) {
        if (customerList == null) {
            customerList = new ArrayList<>();
        }
        customerList.addAll(items);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return customerList != null ? customerList.size() : 0;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View convertView = LayoutInflater.from(mContext).inflate(R.layout.view_customer_item, parent, false);
        return new ViewHolder(convertView);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Customer currentItem = customerList.get(position);
        holder.customerCompany.setText(currentItem.getCompany());
        holder.customerAddress.setText(currentItem.getAddress());
        holder.customerCreatedDate.setText(currentItem.getCreateddate());
        holder.customerContactDetails.setSeparatedItems(Arrays.asList(new String[]{currentItem.getPhone(), currentItem.getEmail()}));
        holder.customerContactDetails.viewSeparatedItems();

        holder.customerDetails.setSeparatedItems(Arrays.asList(new String[]{new StringBuilder()
                .append(currentItem.getName().getFirst())
                .append(" ").append(currentItem.getName().getLast()).toString(), currentItem.getAccountnumber().toString()}));
        holder.customerDetails.viewSeparatedItems();
        RxView.clicks(holder.linearLayout).subscribe(aVoid -> mPresenter.showCustomerEntitlement(customerList.get(position)));
    }


    static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.title_holder)
        SeparatedTextWidget customerDetails;

        @BindView(R.id.customer_contact_details)
        SeparatedTextWidget customerContactDetails;
        @BindView(R.id.customer_company)
        AppCompatTextView customerCompany;

        @BindView(R.id.customer_address)
        AppCompatTextView customerAddress;

        @BindView(R.id.parentPanel)
        LinearLayout linearLayout;

        @BindView(R.id.customer_created_date)
        AppCompatTextView customerCreatedDate;


        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

    }
}
