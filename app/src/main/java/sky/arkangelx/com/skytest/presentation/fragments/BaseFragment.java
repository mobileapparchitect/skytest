package sky.arkangelx.com.skytest.presentation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.ButterKnife;
import butterknife.Unbinder;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import sky.arkangelx.com.skytest.SkyTestApplication;
import sky.arkangelx.com.skytest.datasource.modules.ApplicationModule;
import sky.arkangelx.com.skytest.datasource.modules.DaggerUIComponent;
import sky.arkangelx.com.skytest.datasource.modules.DataModule;
import sky.arkangelx.com.skytest.datasource.modules.UIComponent;
import sky.arkangelx.com.skytest.datasource.modules.UIModule;
import sky.arkangelx.com.skytest.presentation.presenters.BasePresenter;
import sky.arkangelx.com.skytest.presentation.presenters.IView;


public abstract class BaseFragment extends Fragment implements IView {
    private final CompositeDisposable disposables = new CompositeDisposable();
    private Unbinder mUnbinder;
    private View rootView;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(getLayoutResId(), container, false);
        mUnbinder = ButterKnife.bind(this, rootView);
        inject(getUIComponent(rootView));
        setRetainInstance(true);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        final ActionBar supportActionBar = ((AppCompatActivity) getActivity()).getSupportActionBar();
        if (supportActionBar != null) {
            setupSupportActionBar(supportActionBar);
        }
    }

    protected abstract void setupSupportActionBar(ActionBar supportActionBar);



    @Override
    public void onResume() {
        super.onResume();
        resumePresenter();
    }

    @Override
    public void onPause() {
        super.onPause();
        pausePresenter();
    }

    @Override
    public void onDestroyView() {
        mUnbinder.unbind();
        super.onDestroyView();
    }


    protected UIComponent getUIComponent(View rootView) {
        final UIModule uIModule = new UIModule(rootView, this);
        return DaggerUIComponent.builder().dataModule(new DataModule(SkyTestApplication.getInstance())).applicationModule(new ApplicationModule(SkyTestApplication.getInstance())).uIModule(uIModule).build();

    }

    private void resumePresenter() {
        if (getPresenter() != null) {
            getPresenter().onResume();
        }
    }

    private void pausePresenter() {
        if (getPresenter() != null) {
            getPresenter().onPause();
        }
    }

    protected abstract void inject(UIComponent uiComponent);

    protected abstract int getLayoutResId();

    protected abstract BasePresenter getPresenter();


    protected void showErrorMessage(int stringResource) {
        final Snackbar snackbar = Snackbar.make(rootView, getActivity().getResources().getString(stringResource), Snackbar.LENGTH_INDEFINITE);
        snackbar.setAction(android.R.string.ok, v -> snackbar.dismiss());
        snackbar.show();
    }


    @Override
    public void onStop() {
        super.onStop();
        disposables.clear();
    }


    protected final void addDisposable(Disposable disposable) {
        disposables.add(disposable);
    }
}