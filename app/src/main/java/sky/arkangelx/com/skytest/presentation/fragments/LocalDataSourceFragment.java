package sky.arkangelx.com.skytest.presentation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import rx.Observable;
import sky.arkangelx.com.skytest.R;
import sky.arkangelx.com.skytest.SkyTestApplication;
import sky.arkangelx.com.skytest.datasource.model.Customer;
import sky.arkangelx.com.skytest.datasource.modules.UIComponent;
import sky.arkangelx.com.skytest.presentation.RecyclerViewScrollEventObservable;
import sky.arkangelx.com.skytest.presentation.adapters.SkyCustomerAdapter;
import sky.arkangelx.com.skytest.presentation.presenters.BasePresenter;
import sky.arkangelx.com.skytest.presentation.presenters.LocalDataSourceEntitlementPresenter;
import sky.arkangelx.com.skytest.presentation.presenters.SkyCustomerEntitlementView;

/**
 * Created by arkangel on 29/04/2017.
 */

public class LocalDataSourceFragment extends BaseFragment implements SkyCustomerEntitlementView {
    private LinearLayoutManager mLayoutManager;
    private SkyCustomerAdapter skyCustomerAdapter;

    @BindView(R.id.recyclerView)
    RecyclerView recyclerView;
    private int startIndex, endIndex;

    @BindView(R.id.progressBar)
    ProgressBar progressBar;
    @Inject
    LocalDataSourceEntitlementPresenter mLocalDataSourceEntitlementPresenter;


    @Override
    protected void setupSupportActionBar(ActionBar supportActionBar) {

    }

    @Override
    protected void inject(UIComponent uiComponent) {
        uiComponent.inject(this);
    }


    @Override
    protected int getLayoutResId() {
        return R.layout.screen_entitlement;
    }

    @Override
    protected BasePresenter getPresenter() {
        return mLocalDataSourceEntitlementPresenter;
    }

    public static LocalDataSourceFragment getInstance() {
        return new LocalDataSourceFragment();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        recyclerView.setHasFixedSize(false);
        mLayoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(mLayoutManager);
        skyCustomerAdapter = new SkyCustomerAdapter(getActivity(), mLocalDataSourceEntitlementPresenter);
        recyclerView.setAdapter(skyCustomerAdapter);
        RecyclerViewScrollEventObservable.createObservable(recyclerView, mLayoutManager).subscribe(recyclerViewScrollEvent -> mLocalDataSourceEntitlementPresenter.getCustomer(startIndex, endIndex * 2));

    }


    @Override
    public void onPause() {
        super.onPause();
        getPresenter().onPause();

    }

    @Override
    public void onResume() {
        super.onResume();
        getPresenter().onResume();
        startIndex = 0;
        endIndex = 10;
    }


    @Override
    public void onCustomerLoaded(Observable<List<Customer>> skyCustomers) {
        skyCustomers.observeOn(MAIN_THREAD_SCHEDULER).subscribe(customerList -> {
            if (customerList != null && !customerList.isEmpty()) {
                startIndex = endIndex;
                endIndex += customerList.size();
                skyCustomerAdapter.addItems(customerList);
            }
        });
    }

    @Override
    public void onErrorLoadingData(Observable<String> errorMessage) {
        errorMessage.observeOn(MAIN_THREAD_SCHEDULER).subscribe(s -> Toast.makeText(SkyTestApplication.getInstance(), s, Toast.LENGTH_SHORT).show());
    }

    @Override
    public void showCustomerEntitlement(Customer customer) {
        if (customer.getRewardsDatum() != null) {
            Toast.makeText(getContext(), customer.getRewardsDatum().getReward(), Toast.LENGTH_LONG).show();
        } else {
            Toast.makeText(getContext(), customer.getRewardErrorMesage(), Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onDataLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onLoadingEnd() {
        progressBar.setVisibility(View.GONE);
    }


}
