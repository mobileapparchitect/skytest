package sky.arkangelx.com.skytest.presentation.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.view.View;

import butterknife.BindView;
import sky.arkangelx.com.skytest.R;
import sky.arkangelx.com.skytest.datasource.modules.UIComponent;
import sky.arkangelx.com.skytest.presentation.presenters.BasePresenter;

public class SkyCustomerEntitlementTabbedFragment extends BaseFragment {

    private static final int TABS_COUNT = 3;


    @BindView(R.id.viewpager)
    ViewPager viewPager;

    @BindView(R.id.sliding_tabs)
    TabLayout tabLayout;


    public static SkyCustomerEntitlementTabbedFragment getInstance() {
        return new SkyCustomerEntitlementTabbedFragment();
    }


    @SuppressWarnings("ConstantConditions")
    private void setupViews() {
        SectionsPagerAdapter tabsSectionsPagerAdapter = new SectionsPagerAdapter(getChildFragmentManager());
        viewPager.setAdapter(tabsSectionsPagerAdapter);
        viewPager.setOffscreenPageLimit(3);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setText("ASSETS");
        tabLayout.getTabAt(1).setText("FIREBASE");
        tabLayout.getTabAt(2).setText("HTTPS");
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        setupViews();
    }

    @Override
    protected void setupSupportActionBar(ActionBar supportActionBar) {

    }

    @Override
    protected void inject(UIComponent uiComponent) {
        uiComponent.inject(this);
    }

    @Override
    protected int getLayoutResId() {
        return R.layout.tabbed_fragment;
    }


    @Override
    protected BasePresenter getPresenter() {
        return null;
    }

    private static class SectionsPagerAdapter extends FragmentPagerAdapter {

        SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public Fragment getItem(int position) {
            switch (position) {
                case 0:
                    return LocalDataSourceFragment.getInstance();
                case 1:
                    return FirebaseFragment.getInstance();
                case 2:
                    return SkyCustomerEntitlementFragment.getInstance();
            }
            return null;
        }

        @Override
        public int getCount() {
            return TABS_COUNT;
        }
    }
}
