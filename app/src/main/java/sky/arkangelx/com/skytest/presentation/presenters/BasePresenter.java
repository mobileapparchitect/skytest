package sky.arkangelx.com.skytest.presentation.presenters;


import sky.arkangelx.com.skytest.datasource.model.Customer;
import sky.arkangelx.com.skytest.datasource.model.Reward;

import static sky.arkangelx.com.skytest.presentation.presenters.IView.publishRelay;

public abstract class BasePresenter<T extends IView> {


    protected T mView;
    protected SkyCustomerEntitlementView skyCustomerEntitlementView;

    public rx.Observable<IView> getObservable() {
        return publishRelay;
    }


    public BasePresenter(T view) {
        mView = view;
        publishRelay.observeOn(observeOn());

    }

    public abstract void onResume();


    public abstract void onPause();


    protected rx.Scheduler observeOn() {
        return IView.MAIN_THREAD_SCHEDULER;
    }

    public abstract void showCustomerEntitlement(Customer customer);
}