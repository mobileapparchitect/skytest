package sky.arkangelx.com.skytest.presentation.presenters;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;
import sky.arkangelx.com.skytest.datasource.model.Customer;
import sky.arkangelx.com.skytest.datasource.repository.DataRepositoryFactory;
import sky.arkangelx.com.skytest.datasource.repository.FirebaseDataSourceRestClientUseCase;
import sky.arkangelx.com.skytest.datasource.repository.RepoType;

/**
 * Created by arkangel on 23/06/2017.
 */

public class FirebaseDataSourceEntitlementPresenter extends BasePresenter<SkyCustomerEntitlementView> {

    private FirebaseDataSourceRestClientUseCase mFirebaseDataSourceRestClientUseCase;


    public FirebaseDataSourceEntitlementPresenter(SkyCustomerEntitlementView view, DataRepositoryFactory dataRepositoryFactory) {
        super(view);
        skyCustomerEntitlementView = view;
        this.mFirebaseDataSourceRestClientUseCase = (FirebaseDataSourceRestClientUseCase) dataRepositoryFactory.getRepoType(RepoType.FIREBASE_SOURCE);
    }

    @Override
    public void onResume() {
        getCustomer(0, 10);
    }

    @Override
    public void onPause() {

    }

    @Override
    public void showCustomerEntitlement(Customer customer) {
        skyCustomerEntitlementView.showCustomerEntitlement(customer);
    }


    public void getCustomer(int startIndex, int endIndex) {

        this.mFirebaseDataSourceRestClientUseCase.getCustomers(startIndex, endIndex).startWith(commenceLoadingCustomers()).doOnCompleted(() -> endLoading())
                .switchIfEmpty(Observable.empty())
                .onErrorReturn(throwable -> {
                    skyCustomerEntitlementView.onErrorLoadingData(Observable.just("Error Retrieving Data"));
                    return null;
                }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(skyCustomers -> skyCustomerEntitlementView.onCustomerLoaded(Observable.just(skyCustomers)));
    }


    public Observable<List<Customer>> commenceLoadingCustomers() {
        skyCustomerEntitlementView.onDataLoading();
        return Observable.empty();
    }

    public Observable<List<Customer>> endLoading() {
        skyCustomerEntitlementView.onLoadingEnd();
        return Observable.empty();
    }
}

