package sky.arkangelx.com.skytest.presentation.presenters;

import com.jakewharton.rxrelay.BehaviorRelay;

import rx.android.schedulers.AndroidSchedulers;
import sky.arkangelx.com.skytest.datasource.model.Reward;


public interface IView {
    rx.Scheduler MAIN_THREAD_SCHEDULER = AndroidSchedulers.mainThread();
    BehaviorRelay publishRelay = BehaviorRelay.create();


}