package sky.arkangelx.com.skytest.presentation.presenters;

import java.util.List;

import rx.Observable;
import rx.android.schedulers.AndroidSchedulers;
import rx.functions.Action0;
import rx.schedulers.Schedulers;
import sky.arkangelx.com.skytest.datasource.model.Customer;
import sky.arkangelx.com.skytest.datasource.repository.DataRepositoryFactory;
import sky.arkangelx.com.skytest.datasource.repository.LocalDataSourceClientUseCase;
import sky.arkangelx.com.skytest.datasource.repository.RepoType;
import sky.arkangelx.com.skytest.presentation.activities.MainActivity;

/**
 * Created by arkangel on 23/06/2017.
 */

public class LocalDataSourceEntitlementPresenter extends BasePresenter<SkyCustomerEntitlementView> {

    private LocalDataSourceClientUseCase mLocalDataSourceClientUseCase;


    public LocalDataSourceEntitlementPresenter(SkyCustomerEntitlementView view, DataRepositoryFactory dataRepositoryFactory) {
        super(view);
        skyCustomerEntitlementView = view;
        this.mLocalDataSourceClientUseCase = (LocalDataSourceClientUseCase) dataRepositoryFactory.getRepoType(RepoType.LOCAL_SOURCE);
    }

    @Override
    public void onResume() {
        getCustomer(0, 10);
    }

    @Override
    public void onPause() {

    }

    @Override
    public void showCustomerEntitlement(Customer customer) {
        skyCustomerEntitlementView.showCustomerEntitlement(customer);
    }

    public void getCustomer(int startIndex, int endIndex) {

        this.mLocalDataSourceClientUseCase.getCustomers(startIndex, endIndex)
                .switchIfEmpty(Observable.empty())
                .startWith(commenceLoadingCustomers()).observeOn(AndroidSchedulers.mainThread())
                .doOnCompleted(() -> endLoading())
                .onErrorReturn(throwable -> {
                    skyCustomerEntitlementView.onErrorLoadingData(Observable.just("Error Retrieving Data"));
                    return null;
                }).subscribeOn(Schedulers.io()).observeOn(AndroidSchedulers.mainThread())
                .subscribe(skyCustomers -> skyCustomerEntitlementView.onCustomerLoaded(Observable.just(skyCustomers)));
    }

    public Observable<List<Customer>> commenceLoadingCustomers() {
        skyCustomerEntitlementView.onDataLoading();
        return Observable.empty();
    }

    public Observable<List<Customer>> endLoading() {
        skyCustomerEntitlementView.onLoadingEnd();
        return Observable.empty();
    }


}

