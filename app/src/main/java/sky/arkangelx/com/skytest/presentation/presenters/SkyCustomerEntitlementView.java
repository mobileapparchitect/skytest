package sky.arkangelx.com.skytest.presentation.presenters;

import java.util.List;

import rx.Observable;
import sky.arkangelx.com.skytest.datasource.model.Customer;
import sky.arkangelx.com.skytest.datasource.model.Reward;

/**
 * Created by arkangel on 23/06/2017.
 */

public interface SkyCustomerEntitlementView extends IView {

    void onCustomerLoaded(Observable<List<Customer>> skyCustomers);
    void  onErrorLoadingData(Observable<String> errorMessage);
    void showCustomerEntitlement(Customer customer);
    void onDataLoading();
    void onLoadingEnd();

}
