package sky.arkangelx.com.skytest.utils;


import android.content.Context;

import java.io.IOException;
import java.io.InputStream;


public class FileUtils {


    public static rx.Observable<String> readFileAsStringObservable(Context context, String fileName) {

        InputStream stream = null;
        try {
            stream = context.getAssets().open(fileName);
            int size = stream.available();
            byte[] buffer = new byte[size];
            stream.read(buffer);
            stream.close();
            return rx.Observable.just(new String(buffer));

        } catch (IOException e) {
            e.printStackTrace();
        }



        return null;
    }

}
